//Logica de Backend

//Servidor
const express = require('express');
// Backend con Stripe
const Stripe = require('stripe');
//Comunicar los dos servidores
const cors = require('cors');

//llave privada
// Recibe un Objeto y obtenemos la instancia
const stripe = new Stripe("sk_test_51HUjgnAk18NAkeWAXStHr1zq438kenXYl43aVqnkfHuiwGDPKNnFfZoAc4S1TA9H9WW6sVTvjtnVEy8IlebZvT6P00ahnMMyXa");

const app = express();
app.use(cors({origin: 'http://localhost:3000'}));
app.use(express.json());

app.post('/api/checkout', async (req, res) => {

        try {
            const {id, amount} = req.body

            //Comfirmamos el pago
            const payment = await stripe.paymentIntents.create({
                amount,
                currency: "USD",
                description: "Gaming KeyBoard",
                payment_method: id,
                confirm: true
            });

            console.log(payment)


            // console.log(req.body)
            res.send({message: 'Pago Exitoso'})
        }catch (e) {
            console.log(e)
            res.json({message: e.raw.message})

        }
})


// * node server/index.js
app.listen(3007, () => {
    console.log('Server on Port ', 3007)
})

